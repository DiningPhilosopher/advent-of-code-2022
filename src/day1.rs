#[aoc_generator(day1)]
fn input_generator(input: &str) -> Vec<Vec<usize>> {
    input
        .split("\n\n")
        .map(|str_vec| {
            str_vec
                .lines()
                .map(|str| str.parse::<usize>().unwrap())
                .collect()
        })
        .collect()
}

fn total_per_elf(unsummed: &[Vec<usize>]) -> Vec<usize> {
    unsummed
        .iter()
        .map(|calory_vec| calory_vec.iter().sum())
        .collect()
}

#[aoc(day1, part1)]
fn solve_part1(input: &[Vec<usize>]) -> usize {
    *total_per_elf(input).iter().max().unwrap()
}

#[aoc(day1, part2)]
fn solve_part2(input: &[Vec<usize>]) -> usize {
    let mut sorted_calories = total_per_elf(input);
    sorted_calories.sort();
    sorted_calories.reverse();
    sorted_calories[..3].iter().sum()
}
