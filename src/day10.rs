enum Op {
    Noop,
    Addx(i32),
}

#[aoc_generator(day10)]
fn input_generator(input: &str) -> Vec<Op> {
    input
        .lines()
        .map(|str| match &str[0..4] {
            "noop" => Op::Noop,
            "addx" => Op::Addx(str[5..].parse().unwrap()),
            _ => panic!("Unexpected opcode"),
        })
        .collect()
}

fn added_signals(signal: &[i32], ticks: &[i32]) -> i32 {
    let mut result = 0;
    for tick in ticks {
        result += tick * signal[(tick - 1) as usize];
    }
    result
}

fn create_signal(input: &[Op]) -> Vec<i32> {
    let mut signal: Vec<i32> = vec![1];
    for i in input {
        signal.push(*signal.last().unwrap());
        match i {
            Op::Noop => (),
            Op::Addx(sig) => {
                signal.push(signal.last().unwrap() + sig);
            }
        }
    }
    signal
}

#[aoc(day10, part1)]
fn solve_part1(input: &[Op]) -> i32 {
    let signal = create_signal(input);
    added_signals(&signal, &[20, 60, 100, 140, 180, 220])
}

#[aoc(day10, part2)]
// cargo aoc requires an output, this is just a dummy in this case
// since we read the ASCII-art answer from the terminal output.
fn solve_part2(input: &[Op]) -> usize {
    let signal = create_signal(input);
    for r in 0..6 {
        for c in 0..40 {
            if c == signal[(c + 40 * r) as usize] - 1
                || c == signal[(c + 40 * r) as usize]
                || c == signal[(c + 40 * r) as usize] + 1
            {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }
    0
}

#[cfg(test)]
mod tests {
    use std::fs;

    use super::*;
    #[test]
    fn test_day_10_p1() {
        let input =
            fs::read_to_string("./src/day10_test_input.txt").expect("Error opening test file");
        assert_eq!(solve_part1(&input_generator(&input)), 13140);
    }
}
