#[derive(PartialEq, Clone, Copy)]
enum RPS {
    R,
    P,
    S,
}

#[aoc_generator(day2)]
fn input_generator(input: &str) -> Vec<(RPS, RPS)> {
    input
        .lines()
        .map(|x| {
            let mut round_chars = x.chars();
            let elf_play = match round_chars.next().unwrap() {
                'A' => RPS::R,
                'B' => RPS::P,
                'C' => RPS::S,
                _ => panic!("Elves should only use ABC for playing"),
            };
            let col_2 = match round_chars.nth(1).unwrap() {
                'X' => RPS::R,
                'Y' => RPS::P,
                'Z' => RPS::S,
                _ => panic!("We should only use XYZ for playing"),
            };
            (elf_play, col_2)
        })
        .collect()
}

fn score(round: &(RPS, RPS)) -> usize {
    let elf = round.0;
    let us = round.1;
    let mut score = 0;
    match us {
        RPS::R => score += 1,
        RPS::P => score += 2,
        RPS::S => score += 3,
    }
    if elf == us {
        score += 3;
    } else if us == RPS::R && elf == RPS::S
        || us == RPS::P && elf == RPS::R
        || us == RPS::S && elf == RPS::P
    {
        score += 6;
    }
    score
}

#[aoc(day2, part1)]
fn solve_part1(input: &[(RPS, RPS)]) -> usize {
    input.iter().map(score).sum()
}

fn score_2(round: &(RPS, RPS)) -> usize {
    let us = match round.1 {
        RPS::R => match round.0 {
            // lose
            RPS::P => RPS::R,
            RPS::S => RPS::P,
            RPS::R => RPS::S,
        },
        RPS::P => round.0, // draw
        RPS::S => match &round.0 {
            // win
            RPS::P => RPS::S,
            RPS::S => RPS::R,
            RPS::R => RPS::P,
        },
    };
    score(&(round.0, us))
}

#[aoc(day2, part2)]
fn solve_part2(input: &[(RPS, RPS)]) -> usize {
    input.iter().map(score_2).sum()
}
