#[aoc_generator(day3)]
fn input_generator(input: &str) -> Vec<String> {
    input.lines().map(|x| x.to_string()).collect()
}

fn split_str(inp: &String) -> (&str, &str) {
    (&inp[..inp.len() / 2], &inp[inp.len() / 2..])
}

fn find_common(strings: &[String]) -> char {
    for c in strings[0].chars() {
        let mut count = 1;
        for other_str in &strings[1..] {
            if other_str.contains(c) {
                count += 1;
                continue;
            }
        }
        if count == strings.len() {
            return c;
        }
    }
    unreachable!()
}

fn priority(the_char: char) -> usize {
    if the_char.is_lowercase() {
        return 1 + the_char as usize - 'a' as usize;
    }
    if the_char.is_uppercase() {
        return 27 + the_char as usize - 'A' as usize;
    }
    unreachable!()
}

#[aoc(day3, part1)]
fn solve_part1(input: &[String]) -> usize {
    let mut score = 0;
    for line in input {
        let parts = split_str(line);
        let common_char = find_common(&[parts.0.to_string(), parts.1.to_string()]);
        score += priority(common_char);
    }
    score
}

#[aoc(day3, part2)]
fn solve_part2(input: &[String]) -> usize {
    let mut score = 0;
    for chunk in input.chunks(3) {
        let common_char = find_common(chunk);
        score += priority(common_char);
    }
    score
}
