#[aoc_generator(day4)]
fn input_generator(input: &str) -> Vec<Vec<Vec<usize>>> {
    let mut ranges: Vec<Vec<Vec<usize>>> = vec![];
    for line in input.lines() {
        let pair: Vec<Vec<usize>> = line
            .split(',')
            .map(|x| {
                x.split('-')
                    .map(|num_str| num_str.parse::<usize>().unwrap())
                    .collect()
            })
            .collect();
        ranges.push(pair);
    }
    ranges
}

fn encloses(container: &[usize], contained: &[usize]) -> bool {
    container[0] <= contained[0] && container[1] >= contained[1]
}

fn overlaps(left: &[usize], right: &[usize]) -> bool {
    (left[0] >= right[0] && left[0] <= right[1])
        || (left[1] >= right[0] && left[1] <= right[1])
        || (right[0] >= left[0] && right[0] <= left[1])
        || (right[1] >= left[0] && right[1] <= left[1])
}

#[aoc(day4, part1)]
fn solve_part1(input: &[Vec<Vec<usize>>]) -> usize {
    let mut count = 0;
    for pair in input {
        if encloses(&pair[0], &pair[1]) || encloses(&pair[1], &pair[0]) {
            count += 1;
        }
    }
    count
}

#[aoc(day4, part2)]
fn solve_part2(input: &[Vec<Vec<usize>>]) -> usize {
    let mut count = 0;
    for pair in input {
        if overlaps(&pair[0], &pair[1]) {
            count += 1;
        }
    }
    count
}
