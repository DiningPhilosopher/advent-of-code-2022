type Stack = Vec<char>;

#[derive(Debug, Clone)]
struct Instruction {
    from: usize,
    to: usize,
    count: usize,
}

#[aoc_generator(day5)]
fn input_generator(input: &str) -> (Vec<Stack>, Vec<Instruction>) {
    let mut stacks_and_instructions = input.split("\n\n");
    let mut stacks: Vec<Stack> = vec![];
    for l in stacks_and_instructions
        .next()
        .unwrap()
        .lines()
        .rev()
        .skip(1)
    {
        let mut stack_idx: usize = 0;
        let mut chr = l.chars().nth(1 + 4 * stack_idx);
        while chr != None {
            if chr == Some(' ') {
                stack_idx += 1;
                chr = l.chars().nth(1 + 4 * stack_idx);
                continue;
            }
            if stacks.get(stack_idx) == None {
                stacks.push(vec![]);
            }
            let stack_to_insert = stacks.get_mut(stack_idx).unwrap();
            stack_to_insert.push(chr.unwrap());
            stack_idx += 1;
            chr = l.chars().nth(1 + 4 * stack_idx);
        }
    }
    let mut instrs = Vec::<Instruction>::new();
    for l in stacks_and_instructions.next().unwrap().lines() {
        let mut is = l.split(' ');
        instrs.push(Instruction {
            count: is.nth(1).unwrap().parse().unwrap(),
            from: is.nth(1).unwrap().parse().unwrap(),
            to: is.nth(1).unwrap().parse().unwrap(),
        });
    }
    (stacks, instrs)
}

fn apply_instruction_9000(stacks: &mut [Stack], inst: Instruction) {
    for _ in 0..inst.count {
        let popped = stacks[inst.from - 1].pop().unwrap();
        stacks[inst.to - 1].push(popped);
    }
}

fn apply_instruction_9001(stacks: &mut [Stack], inst: Instruction) {
    let mut package = Stack::new();
    for _ in 0..inst.count {
        let popped = stacks[inst.from - 1].pop().unwrap();
        package.push(popped)
    }
    package.reverse();
    stacks[inst.to - 1].append(&mut package);
}

fn read_answer(stacks: &[Stack]) -> String {
    let mut answer = Vec::<char>::new();
    for s in stacks {
        answer.push(s[s.len() - 1]);
    }
    answer.into_iter().collect()
}

#[aoc(day5, part1)]
fn solve_part1(input: &(Vec<Stack>, Vec<Instruction>)) -> String {
    let (mut stacks, instr) = (input.0.clone(), input.1.clone());
    for i in instr {
        apply_instruction_9000(&mut stacks, i);
    }
    read_answer(&stacks)
}

#[aoc(day5, part2)]
fn solve_part2(input: &(Vec<Stack>, Vec<Instruction>)) -> String {
    let (mut stacks, instr) = (input.0.clone(), input.1.clone());
    for i in instr {
        apply_instruction_9001(&mut stacks, i);
    }
    read_answer(&stacks)
}
