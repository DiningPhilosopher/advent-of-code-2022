fn all_unique(chrs: &str) -> bool {
    for chr in chrs.chars() {
        if chrs.matches(chr).count() > 1 {
            return false;
        }
    }
    true
}

#[aoc(day6, part1)]
fn solve_part1(input: &str) -> usize {
    let mut idx = 0;
    loop {
        let window = input.get(idx..idx + 4).unwrap();
        if all_unique(window) {
            return idx + 4;
        }
        idx += 1;
    }
}

#[aoc(day6, part2)]
fn solve_part2(input: &str) -> usize {
    let mut idx = 0;
    loop {
        let window = input.get(idx..idx + 14).unwrap();
        if all_unique(window) {
            return idx + 14;
        }
        idx += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    const INPUT: &str = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";
    #[test]
    fn test1() {
        assert_eq!(solve_part1(INPUT), 7);
    }
}
