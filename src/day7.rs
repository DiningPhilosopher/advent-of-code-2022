use regex::Regex;

#[derive(PartialEq, Default, Debug)]
struct Dir {
    id: usize,
    name: String,
    parent: Option<usize>,
    subdirs: Vec<usize>,
    files: Vec<usize>,
    size: Option<usize>,
}

fn get_dir_mut(dirs: &mut [Dir], idx: usize) -> Option<&mut Dir> {
    dirs.get_mut(idx)
}

fn get_dir(dirs: &[Dir], idx: usize) -> Option<&Dir> {
    dirs.get(idx)
}

fn add_dir(dirs: &mut Vec<Dir>, name: String) -> usize {
    dirs.push(Dir {
        id: dirs.len(),
        name,
        ..Default::default()
    });
    dirs.len() - 1
}

fn get_file(files: &[File], idx: usize) -> Option<&File> {
    files.get(idx)
}

fn add_file(files: &mut Vec<File>, name: String, size: usize) -> usize {
    files.push(File { name, size });
    files.len() - 1
}

fn calc_dir_size(dirs: &mut [Dir], files: &[File], dir_idx: usize) -> usize {
    let mut total_size = 0_usize;
    {
        let dir = get_dir(dirs, dir_idx).unwrap();
        for f_idx in dir.files.iter() {
            total_size += get_file(files, *f_idx).unwrap().size;
        }
    }
    let subdirs;
    {
        let dir = get_dir(dirs, dir_idx).unwrap();
        subdirs = Some(dir.subdirs.clone());
    }
    for subdir_idx in subdirs.unwrap() {
        total_size += calc_dir_size(dirs, files, subdir_idx)
    }
    let dir = get_dir_mut(dirs, dir_idx);
    dir.unwrap().size = Some(total_size);
    total_size
}

fn calc_dir_sizes(dirs: &mut [Dir], files: &[File]) {
    calc_dir_size(dirs, files, 0);
}

#[derive(Default, Debug)]
struct File {
    name: String,
    size: usize,
}

impl PartialEq for File {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

#[aoc_generator(day7)]
fn input_generator(input: &str) -> (Vec<Dir>, Vec<File>) {
    let mut files: Vec<File> = vec![];
    let mut dirs: Vec<Dir> = vec![];
    let mut current_dir_idx: usize = 0;

    let cd_re = Regex::new(r"\$ cd (.+)").unwrap();
    let file_re = Regex::new(r"([0-9]+) (.+)").unwrap();
    let filesys = Dir {
        id: 0,
        name: "/".to_string(),
        ..Default::default()
    };
    dirs.push(filesys);
    let mut current_path = vec![0_usize];
    // Conveniently ignore initial "cd /"
    for line in input.lines().skip(1) {
        if let Some(captures) = cd_re.captures(line) {
            let dir_name = captures.get(1).unwrap().as_str().to_string();
            if let ".." = dir_name.as_str() {
                current_path.pop();
                current_dir_idx = *current_path.last().unwrap();
                continue;
            }
            // Cheeky shortcut: assume cd into dir is done only once
            let new_current_dir_idx = add_dir(&mut dirs, dir_name);
            let current_dir = get_dir_mut(&mut dirs, current_dir_idx).unwrap();
            current_dir.subdirs.push(new_current_dir_idx);
            current_dir_idx = new_current_dir_idx;
            current_path.push(current_dir_idx);
        } else if let Some(captures) = file_re.captures(line) {
            let current_dir = get_dir_mut(&mut dirs, current_dir_idx).unwrap();
            let new_file_idx = add_file(
                &mut files,
                captures.get(2).unwrap().as_str().to_string(),
                captures.get(1).unwrap().as_str().parse().unwrap(),
            );
            current_dir.files.push(new_file_idx);
        }
    }
    calc_dir_sizes(&mut dirs, &files);
    (dirs, files)
}

#[aoc(day7, part1)]
fn solve_part1(input: &(Vec<Dir>, Vec<File>)) -> usize {
    let mut answer = 0_usize;
    for d in input.0.iter() {
        if d.size.unwrap() <= 100000_usize {
            answer += d.size.unwrap();
        }
    }
    answer
}

#[aoc(day7, part2)]
fn solve_part2(input: &(Vec<Dir>, Vec<File>)) -> usize {
    let total_space = 70000000_usize;
    let required_space = 30000000_usize;
    let used_space = input.0.get(0).unwrap().size.unwrap();
    let free_space = total_space - used_space;
    let to_free = required_space - free_space;
    let mut smallest_dir = input.0.get(0).unwrap();
    for dir in input.0.iter() {
        if dir.size.unwrap() >= to_free && dir.size.unwrap() < smallest_dir.size.unwrap() {
            smallest_dir = dir;
        }
    }
    smallest_dir.size.unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    const INPUT: &str = "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";
    #[test]
    fn test_day_7() {
        let testfs = input_generator(INPUT);
        assert_eq!(solve_part1(&testfs), 95437);
    }
}
