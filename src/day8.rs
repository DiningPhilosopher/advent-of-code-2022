fn add_visibility(forest: &mut [Vec<Tree>]) {
    // look from left
    let mut highest;
    for r in forest.iter_mut() {
        highest = -1;
        for tree in r {
            if tree.height > highest {
                tree.visible = true;
                highest = tree.height;
            }
        }
    }
    // look from right
    for r in forest.iter_mut() {
        highest = -1;
        for tree in r.iter_mut().rev() {
            if tree.height > highest {
                tree.visible = true;
                highest = tree.height;
            }
        }
    }
    // look from top
    let mut highest_in_row = vec![-1; forest[0].len()];
    for r in forest.iter_mut() {
        for (idx, tree) in r.iter_mut().enumerate() {
            if tree.height > highest_in_row[idx] {
                highest_in_row[idx] = tree.height;
                tree.visible = true;
            }
        }
    }
    // look from bottom
    let mut highest_in_row = vec![-1; forest[0].len()];
    for r in forest.iter_mut().rev() {
        for (idx, tree) in r.iter_mut().enumerate() {
            if tree.height > highest_in_row[idx] {
                highest_in_row[idx] = tree.height;
                tree.visible = true;
            }
        }
    }
}

fn add_views(forest: &mut Vec<Vec<Tree>>) {
    for row in 0..forest.len() {
        for col in 0..forest[0].len() {
            let house_height = forest[row][col].height;
            let mut dist = 1;
            // Look left
            let mut lscore = 0;
            while let Some(tree) = forest[row].get(col - dist) {
                lscore += 1;
                if tree.height >= house_height {
                    break;
                }
                dist += 1;
            }
            // Look right
            let mut rscore = 0;
            dist = 1;
            while let Some(tree) = forest[row].get(col + dist) {
                rscore += 1;
                if tree.height >= house_height {
                    break;
                }
                dist += 1;
            }
            // Look up
            let mut uscore = 0;
            dist = 1;
            while let Some(tree) = forest.get(row - dist).unwrap_or(&vec![]).get(col) {
                uscore += 1;
                if tree.height >= house_height {
                    break;
                }
                dist += 1;
            }
            // Look down
            let mut dscore = 0;
            dist = 1;
            while let Some(tree) = forest.get(row + dist).unwrap_or(&vec![]).get(col) {
                dscore += 1;
                if tree.height >= house_height {
                    break;
                }
                dist += 1;
            }
            forest[row][col].scenery = lscore * rscore * uscore * dscore;
        }
    }
}

#[aoc_generator(day8)]
fn input_generator(input: &str) -> Vec<Vec<Tree>> {
    let mut forest = vec![];
    for l in input.lines() {
        forest.push(vec![]);
        for c in l.chars() {
            forest.last_mut().unwrap().push(Tree {
                height: c.to_string().parse::<i32>().unwrap(),
                scenery: 0,
                visible: false,
            });
        }
    }
    add_visibility(&mut forest);
    add_views(&mut forest);
    forest
}

#[derive(Debug, Clone)]
struct Tree {
    height: i32,
    scenery: usize,
    visible: bool,
}

#[aoc(day8, part1)]
fn solve_part1(input: &[Vec<Tree>]) -> usize {
    let total_visible = input.iter().flatten().filter(|tree| tree.visible).count();
    total_visible
}

#[aoc(day8, part2)]
fn solve_part2(input: &[Vec<Tree>]) -> usize {
    let best_view = input
        .iter()
        .flatten()
        .max_by(|x, y| x.scenery.cmp(&y.scenery))
        .unwrap();
    best_view.scenery as usize
}

#[cfg(test)]
mod tests {
    use super::*;
    const INPUT: &str = "30373
25512
65332
33549
35390";
    #[test]
    fn test_day_8() {
        assert_eq!(solve_part1(&input_generator(INPUT)), 21);
    }
}
