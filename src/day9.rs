use std::{
    collections::HashSet,
    ops::{Add, Sub},
};

use itertools::Itertools;

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
struct Planck {
    x: i32,
    y: i32,
}

impl Add for Planck {
    type Output = Self;
    fn add(self, other: Self) -> Self::Output {
        Planck {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Add for &Planck {
    type Output = Planck;
    fn add(self, other: Self) -> Self::Output {
        Planck {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Planck {
    type Output = Self;
    fn sub(self, other: Self) -> Self::Output {
        Planck {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

#[aoc_generator(day9)]
fn input_generator(input: &str) -> Vec<Planck> {
    input
        .lines()
        .flat_map(|line| {
            let dist = line
                .chars()
                .skip(2)
                .collect::<String>()
                .parse::<usize>()
                .unwrap();
            match line.chars().next().unwrap() {
                'U' => vec![Planck { x: 0, y: -1 }; dist],
                'D' => vec![Planck { x: 0, y: 1 }; dist],
                'L' => vec![Planck { x: -1, y: 0 }; dist],
                'R' => vec![Planck { x: 1, y: 0 }; dist],
                _ => panic!("First char of each input line must be in UDLR"),
            }
        })
        .collect()
}

fn pull_tail(th: &mut Planck, pull: &Planck, tailpos: &mut Planck, trail: &mut Vec<Planck>) {
    // Move the head
    *th = th.clone() + pull.clone();
    // Move the tail in the direction of the head
    // right
    if th.x == 2 && th.y == 0 {
        th.x = 1;
        tailpos.x += 1;
        trail.push(tailpos.clone());
    }
    // left
    if th.x == -2 && th.y == 0 {
        th.x = -1;
        tailpos.x -= 1;
        trail.push(tailpos.clone());
    }
    // up
    if th.x == 0 && th.y == -2 {
        th.y = -1;
        tailpos.y -= 1;
        trail.push(tailpos.clone());
    }
    // down
    if th.x == 0 && th.y == 2 {
        th.y = 1;
        tailpos.y += 1;
        trail.push(tailpos.clone());
    }
    // up right
    if th.x > 0 && th.y < 0 && th.x.abs() + th.y.abs() > 2 {
        th.x -= 1;
        th.y += 1;
        tailpos.x += 1;
        tailpos.y -= 1;
        trail.push(tailpos.clone());
    }
    // down right
    if th.x > 0 && th.y > 0 && th.x.abs() + th.y.abs() > 2 {
        th.x -= 1;
        th.y -= 1;
        tailpos.x += 1;
        tailpos.y += 1;
        trail.push(tailpos.clone());
    }
    // up left
    if th.x < 0 && th.y < 0 && th.x.abs() + th.y.abs() > 2 {
        th.x += 1;
        th.y += 1;
        tailpos.x -= 1;
        tailpos.y -= 1;
        trail.push(tailpos.clone());
    }
    // down left
    if th.x < 0 && th.y > 0 && th.x.abs() + th.y.abs() > 2 {
        th.x += 1;
        th.y -= 1;
        tailpos.x -= 1;
        tailpos.y += 1;
        trail.push(tailpos.clone());
    }
}

fn trail_to_steps(trail: &[Planck]) -> Vec<Planck> {
    let mut steps = Vec::<Planck>::new();
    for (prev, next) in trail.iter().tuple_windows() {
        steps.push(prev.clone() - next.clone());
    }
    steps
}

#[allow(dead_code)]
fn print_map(map: &HashSet<Planck>) {
    let mut center = vec![];
    const RADIUS: i32 = 10;
    for _ in 0..2 * RADIUS {
        center.push(vec![]);
        for _ in 0..2 * RADIUS {
            center.last_mut().unwrap().push('.');
        }
    }
    for p in map {
        if -RADIUS < p.x && RADIUS > p.x && -RADIUS < p.y && RADIUS > p.y {
            center[(p.y + RADIUS) as usize][(p.x + RADIUS) as usize] = '#';
        }
    }
    for r in &center {
        for cell in r {
            print!("{cell}");
        }
        println!();
    }
}

#[aoc(day9, part1)]
fn solve_part1(planck_steps: &[Planck]) -> usize {
    let mut t_to_h = Planck { x: 0, y: 0 };
    let mut tail_trail = Vec::<Planck>::new();
    let mut tailpos = Planck { x: 0, y: 0 };
    for i in planck_steps {
        pull_tail(&mut t_to_h, i, &mut tailpos, &mut tail_trail);
    }
    let tailset: HashSet<Planck> = tail_trail.into_iter().collect();
    tailset.len()
}

#[aoc(day9, part2)]
fn solve_part2(planck_steps: &[Planck]) -> usize {
    // Let knot N pull knot N+1 and record the path of N+1.
    // Then convert the path into new steps and repeat until the 10th knot.
    let mut t_to_h;
    let mut tail_trail = vec![];
    let mut planck_steps: Vec<Planck> = planck_steps.to_vec();
    for _ in 0..9 {
        let mut tailpos = Planck { x: 0, y: 0 };
        t_to_h = Planck { x: 0, y: 0 };
        tail_trail = vec![tailpos.clone()];
        for i in &planck_steps {
            pull_tail(&mut t_to_h, i, &mut tailpos, &mut tail_trail);
        }
        planck_steps = trail_to_steps(&tail_trail);
    }
    let tailset: HashSet<Planck> = tail_trail.into_iter().collect();
    // print_map(&tailset);
    tailset.len()
}

#[cfg(test)]
mod tests {
    use super::*;
    const INPUT: &str = "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";
    #[test]
    fn test_day_9_p1() {
        assert_eq!(solve_part1(&input_generator(INPUT)), 13);
    }
    const INPUT2: &str = "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20";
    #[test]
    fn test_day_9_p2() {
        assert_eq!(solve_part2(&input_generator(INPUT2)), 36);
    }
}
